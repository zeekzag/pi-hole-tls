# Pi-hole TLS (feat. Stubby)

This is my simple, containerized setup to get Pi-hole and TLS running with as little configuration needed as possible. This config works great with services like NextDNS, Cloudflare or Quad9. You can, of course, choose your own DNS provider with TLS support.

I decided to post it beacuse there's a ton of configs that make Pi-hole work with Unbound but little to none that combine Pi-hole with Stubby.

**Things to remember when using this compose file:**
- Pi-hole web interface runs on port 9024. Change it to avoid conflicts if you want to run other services that use this port.
- Double check the IP addresses of your docker subnet to make sure you're not trying to use a network that already uses the same address pool.
- Copy the stubby.yml config file to ~/dns/ and choose your DNS provider (you can choose a different location but remember to adjust your docker-compose.yml file accordingly). Config available in the repo (*stubby.yml*) is set to Quad9 (_9.9.9.9_) by default. I preconfigured some other popular resolvers with DoT enabled by default (CF, Mullvad, NextDNS).
- Remember to fill out the pihole-tls.env file before you deploy this compose.

I used the official [Pi-hole](https://hub.docker.com/r/pihole/pihole/) image and my own containerized version of [Stubby](https://hub.docker.com/r/zeekzag/stubby).

# Changelog
**[18.01.2024]:**
- Switched to my own Stubby image (0.4.3).
- Adjusted docker-compose.yml once more. Tried to add as many comments as possible so new users know what's what.

**[24.03.2023]:**
- New image for Stubby (v0.4.2 by Luckyturtledev), should work on both x86-64 and ARM now.
- New example config for Stubby as the old one was quite outdated.
- Adjusted docker-compose.yml, switched to long syntax when able to make stuff easier to read. Container binds aren't all over the place anymore.

**[14.02.2023]:**
- Intitial release
